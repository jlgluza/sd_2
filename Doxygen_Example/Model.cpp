/******************************************************************************
 *                                                                            *
 * file5                                                                      *
 *                                                                            *
 ******************************************************************************/

#include <fstream>
#include <utility>
#include <algorithm>
#include "Model.hpp"

/******************************************************************************
 *                                                                            *
 * constructor                                                                *
 *                                                                            *
 ******************************************************************************/

Model::Model(const string& fields_file_name, const string& vertices_file_name)
{
  ifstream fields_file(fields_file_name.c_str());
  if (!fields_file)
    {
      cerr << "The field list file \"" << fields_file_name
	   << "\" could not be opened\n";

      exit(1);
    }

  read_fields(fields_file);
  fields_file.close();

  ifstream vertices_file(vertices_file_name.c_str());
  if (!vertices_file)
    {
      cerr << "The vertex list file \"" << vertices_file_name
	   << "\" could not be opened\n";

      exit(1);
    }

  read_vertices(vertices_file);
  vertices_file.close();
}

/******************************************************************************
 *                                                                            *
 * destructor                                                                 *
 *                                                                            *
 ******************************************************************************/

Model::~Model()
{
  for (map<const string, Field*>::iterator f = _fields.begin();
       f != _fields.end(); ++f)
    {
      delete f->second;
    }
  
  for (vector<Vertex*>::iterator v = _vertices.begin();
       v != _vertices.end(); ++v)
    {
      delete *v;
    }
}

/******************************************************************************
 *                                                                            *
 * read_fields                                                                *
 *                                                                            *
 ******************************************************************************/

/**
 *
 * Reads the field data from a given input stream. Records are separated by
 * newlines. Empty lines and lines starting with a # symbol are neglected.
 * The format of a record is the following
 *
 * 1) particle name,
 *
 * 2) anti-particle name,
 *
 * 3) the name to be displayed when a diagram line has the field's propagator,
 *
 * 4) field type (currently allowed are Scalar, Vector, Fermion and Ghost),
 *
 * 5) field mass.
 *
 * Record's fields can be separated by spaces or tabs.
 *
 */

void
Model::read_fields(istream& input)
{
  const char delimiters[] ="\r\t ";

  while (input)
    {
      // every record different from a comment or empty line is parsed into
      // identifiers according to the delimiter list.
  
      string buffer;
      getline(input, buffer);

      int begin, end;
      if ((begin = buffer.find_first_not_of(delimiters)) < 0 ||
	  buffer[begin] == '#') continue;

      vector<string> identifier;
      do
	{
	  end = buffer.find_first_of(delimiters, begin);

	  int n_chars = end-begin;
	  if (end < 0) n_chars = end;

	  identifier.push_back(string(buffer, begin, n_chars));
	}
      while ((begin = buffer.find_first_not_of(delimiters, end)) >= 0);
      
      // fields after the 5th are ignored and can also be used as comments
      // although it may happen that in future more information will be 
      // required (e.g. for Majorana fermions).

      if (identifier.size() < 5)
	{
	  cerr << "Incomplete field definition: ";
	  for (vector<string>::iterator id = identifier.begin();
	       id != identifier.end(); ++id)
	    {
	      cerr << *id << " ";
	    }
	  cerr << endl;
	  
	  exit(1);
	}
      
      if (find(identifier[0]) != 0 || find(identifier[1]) != 0)
	{
	  cerr << "The field " << identifier[0] << " or " << identifier[1]
	       << " has already been defined\n";

	  exit(1);
	}

      // The identification of the field type is base only on the first letter.
      // There is also no dependence on the case.
      
      FieldType type;
      switch (identifier[3][0])
	{
	case 's':
	case 'S':
	  type = Scalar;
	  break;
	case 'f':
	case 'F':
	  type = Fermion;
	  break;
	case 'v':
	case 'V':
	  type = Vector;
	  break;
	case 'g':
	case 'G':
	  type = Ghost;
	  break;
	default:
	  cerr << "Unknown field type: " << identifier[3] << endl;
	  exit(1);
	}

      // The insertion into the field map takes care of the possible identity
      // between the particle and the anti-particle.
      
      Field* particle = new Field;

      particle->_identifier = identifier[0];
      particle->_type = type;
      particle->_mass = identifier[4];
      particle->_name = identifier[2];
      particle->_particle = true;
      particle->_coupled_field = particle;

      _fields.insert(make_pair(identifier[0], particle));

      if (identifier[0] != identifier[1])
	{
	  Field* anti_particle = new Field;

	  anti_particle->_identifier = identifier[1];
	  anti_particle->_type = type;
	  anti_particle->_mass = identifier[4];
	  anti_particle->_name = identifier[2];
	  anti_particle->_particle = false;
	  anti_particle->_coupled_field = particle;
	  particle->_coupled_field = anti_particle;

	  _fields.insert(make_pair(identifier[1], anti_particle));
	}
    }
}

/******************************************************************************
 *                                                                            *
 * vertex type map                                                            *
 *                                                                            *
 ******************************************************************************/

const int n_vertex_types = 16;

/**
 *
 * The determination of vertex types based on the vertex field content requires
 * a map from a vector of field type multiplicities to the vertex type and
 * number of values. This map is created by read_vertices() from the 
 * vertex_key, vertex_type, vertex_value_count matrices.
 *
 */

static const int vertex_key[] =
{
  3, 0, 0, 0, // Scalar, Ghost, Fermion, Vector
  4, 0, 0, 0,
  1, 2, 0, 0,
  1, 0, 2, 0,
  1, 0, 0, 2,
  2, 0, 0, 1,
  2, 0, 0, 2,
  0, 0, 0, 3,
  0, 0, 0, 4,
  0, 2, 0, 1,
  0, 0, 2, 1,
  2, 0, 0, 0,
  0, 2, 0, 0,
  0, 0, 2, 0,
  0, 0, 0, 2,
  1, 0, 0, 1
};

/// see vertex_key

static const VertexType vertex_type[] =
{
  Identity,
  Identity,
  Identity,
  FermionScalar,
  VectorScalar,
  DerivativeVectorScalar,
  VectorScalar,
  TripleVector,
  QuadrupleVector,
  GhostVector,
  FermionVector,
  ScalarCT,
  ScalarCT,
  FermionCT,
  VectorCT,
  VectorScalarCT
};

/// see vertex_key

static const int n_vertex_values[] =
{
  1,
  1,
  1,
  2,
  1,
  1,
  1,
  1,
  1,
  1,
  2,
  2,
  2,
  4,
  2,
  1
};

/******************************************************************************
 *                                                                            *
 * read_vertices                                                              *
 *                                                                            *
 ******************************************************************************/

/**
 *
 * Reads the vertices from the input stream. every record has the following
 * format
 * 
 * 1) space or tab separated list of vertex' field names,
 *
 * 2) on the same line the counterterm order in parentheses,
 *
 * 3) starting on a new line, a list of values terminated by a semicolon
 *    according to the required number for the given vertex type.
 *
 * empty lines and lines starting with a # symbol are ignored.
 *
 */

void
Model::read_vertices(istream& input)
{
  // create the vertex type map

  map<vector<int>, pair<VertexType, int> > type_map;

  for (int i = 0; i < n_vertex_types; ++i)
    {
      pair<VertexType, int> type = make_pair(vertex_type[i],n_vertex_values[i]);

      vector<int> key;
      for (int j = 0; j < n_field_types; ++j)
	key.push_back(vertex_key[i*n_field_types+j]);

      type_map[key] = type;
    }

  // read the vertices
	
  const char delimiters[] ="\r\t ";

  while (input)
    {
      Vertex* v = new Vertex;

      // read the first line of the record. it should contain the fields of
      // the vertex and the order of the vertex at the end (between parentheses)
      
      string buffer;
      getline(input, buffer);

      int begin, end;
      if ((begin = buffer.find_first_not_of(delimiters)) < 0 ||
	  buffer[begin] == '#') continue;
      
      vector<string> identifier;
      do
	{
	  end = buffer.find_first_of(delimiters, begin);

	  int n_chars = end-begin;
	  if (end < 0) n_chars = end;

	  identifier.push_back(string(buffer, begin, n_chars));
	}
      while ((begin = buffer.find_first_not_of(delimiters, end)) >= 0);

      // the identification of the vertex requires the key made of 
      // multiplicities of the different field types inside the vertex

      vector<int> key(n_field_types);

      // the vertex degree (the last identifier is the vertex order)
      
      const int d = identifier.size()-1;

      // determine the vertex fields

      for (int i = 0; i < d; ++i)
	{
	  map<const string, Field*>::iterator f = _fields.find(identifier[i]);
	  
	  if (f == _fields.end())
	    {
	      cerr << "There is no field " << identifier[i]
		   << " in the field list\n";

	      exit(1);
	    }

	  ++key[f->second->_type];
	  v->_original_fields.push_back(f->second);
	  v->_sorted_fields.push_back(f->second);
	}
      
      sort(v->_sorted_fields.begin(), v->_sorted_fields.end());
      
      // determine the type of the vertex

      map<vector<int>, pair<VertexType, int> >::iterator t = type_map.find(key);
      if (t == type_map.end())
	{
	  cerr << "Unknown vertex type: ";
	  for (vector<string>::iterator id = identifier.begin();
	       id != identifier.end(); ++id)
	    {
	      cerr << *id << " ";
	    }
	  cerr << endl;
	  
	  exit(1);
	}
      
      v->_type = t->second.first;

      // determine the order of the vertex

      begin = identifier[d].find_first_not_of("(\t ");
      end = identifier[d].find_first_of(")\t ", begin);
      v->_order = atoi(string(identifier[d], begin, end-begin).c_str());

      // read the value of the vertex

      for (int i = 0; i < t->second.second; ++i)
	{
	  string value;
	  bool first_line = true;
	  
	  do
	    {
	      getline(input, buffer);
	      
	      if ((begin = buffer.find_first_not_of(delimiters)) < 0 ||
		  buffer[begin] == '#')
		{
		  end = -1;
		  continue;
		}
	      
	      end = buffer.find_first_of(";");
	      
	      int n_chars = end-begin;
	      if (end < 0) n_chars = end;
	      
	      if (first_line)
		first_line = false;
	      else
		value += "\n\t   ";

	      value.append(buffer, begin, n_chars);
	    }
	  while (end < 0);

	  v->_value.push_back(value);
	}
      
      // update the field list and determine the symmetry factor

      v->_symmetry_factor = 1;
      int current_symmetry_factor = 1;
      for (vector<field_pointer>::iterator f = v->_sorted_fields.begin();
	   f != v->_sorted_fields.end(); ++f)
	{
	  if (f != v->_sorted_fields.begin() && *f == *(f-1))
	    {
	      v->_symmetry_factor *= ++current_symmetry_factor;
	      continue;
	    }
	  else
	    current_symmetry_factor = 1;
	    
	  Field* field = const_cast<Field*>(*f);
	  int m= count(v->_sorted_fields.begin(),v->_sorted_fields.end(),field);
	  
	  if (field->_vertices.size() <= d) field->_vertices.resize(d+1);
	  if (field->_vertices[d].size() <= m) field->_vertices[d].resize(m+1);

	  for (int k = 1; k <= m; ++k) field->_vertices[d][k].insert(v);
	}
      
      // update the vertex list

      _vertices.push_back(v);
    }
}
