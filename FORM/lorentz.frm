*tut p.118

Symbols t, x, y, z, v, c, gamma, [c^2-v^2];
Local X = gamma*(x-v*t);
Local Y = y;
Local Z = z;
Local T = c*gamma*(t-v*x/c^2);
Local S = X^2 + Y^2 + Z^2 - T^2;
id gamma^2 = c^2/[c^2-v^2];
repeat;
id c^2/[c^2-v^2] = 1 + v^2/[c^2-v^2];
endrepeat;
Print S;
.end