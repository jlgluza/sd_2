#-
*jg, 15.05.2008

Off Statistics;
Symbol d;
Dimension d;
Vectors q,q1,...,q9,p,p1,...,p5,k1,...,k2;
Symbols e,Qle,Mle,s,t,mle,mlm;
Symbols S,T,S0,T0,eps;
Symbols a,b,m,Ql,GVe,GAe,num1,...,num9;
Symbols s12,s15;
Symbols m1;
Indices mu1,...,mu5;
Functions F,FF,VV;
Functions gamma,gamma5,gameins;
CFunction Pr;

************************
*Opening tree amplitude*
************************
#include mumugammaTree.in #n1

Local tree1=Rq*`FERMIONCHANNEL';
*'

.sort
Drop Rq;
#if 0
print;
.end
#endif

*changing to tree channels
mul replace_(S,S0);

*shifting numbers of fermion lines
id F(a?,1,?b)=F(a,8,?b);
id F(a?,2,?b)=F(a,9,?b);
id FF(a?,1,?b)=FF(a,8,?b);
id FF(a?,2,?b)=FF(a,9,?b);

********************
*Tree1 amplitude END*
********************
.sort

************************
*second tree amplitude*
************************
#include mumugammaTree.in #n1

Local tree2=Rq*`FERMIONCHANNEL';

.sort
Drop Rq;

********************
*tree1 amplitude END*
********************
.sort

#if 1

#include tr1FeynRules.frm
id gameins(?m1)=1;

b gamma;
#if 0
print +s;
.end
#endif
#endif
.sort
*********************
*Multiply: tree1*tree2*
*********************
mul replace_(Qle,1);
*print;
.sort
Local Amp=i_*tree1*tree2;

.sort
Drop tree1,tree2;

#include tr1FeynRules.frm
id gameins(?m1)=1;

#if 0
b gamma;
print +s;
.end
#endif

***********************************************************
***          S*S channel                                ***
***********************************************************

if(match(S*S0));

	repeat;
	id gamma(9,p1?)*gamma(1,p2?)=gamma(1,p2)*gamma(9,p1);
	id gamma(9,p1?)*gamma(1,mu2?)=gamma(1,mu2)*gamma(9,p1);
	id gamma(9,mu1?)*gamma(1,mu2?)=gamma(1,mu2)*gamma(9,mu1);
	id gamma(9,mu1?)*gamma(1,p2?)=gamma(1,p2)*gamma(9,mu1);
	endrepeat;

#if 0
endif;
b gamma,k1;
print +s;
.end
#endif

********************
*completeness relations*
********************
	mul, left, (gamma(10,p4)-mlm);
	repeat;
	id gamma(10,p4)*gamma(8,p2?)=gamma(8,p2)*gamma(10,p4);
	id gamma(10,p4)*gamma(8,mu1?)=gamma(8,mu1)*gamma(10,p4);
	endrepeat;

	mul, left, (gamma(11,p2)+mlm);
	id gamma(m1?{8,10,11},?p1)=gamma(1,?p1);
	id gamma(1,p1?)=g_(1,p1);
	id gamma(1,mu1?)=g_(1,mu1);
	
***First trace***
	tracen, 1;

#if 0
endif;
id Pr(?p1)=1;
b gamma,k1;
print +s;
.end
#endif

*********************************************

mul, left, (gamma(12,p2)-mle);

#if 0
endif;
id Pr(?p1)=1;
b gamma,k1;
print +s;
.end
#endif

	mul, right, (gamma(13,p1)+mle);
	repeat;
	id gamma(2,p2?)*gamma(13,p1)=gamma(13,p1)*gamma(2,p2);
	id gamma(2,mu1?)*gamma(13,p1)=gamma(13,p1)*gamma(2,mu1);
	endrepeat;

#if 0
endif;
id Pr(?p1)=1;
b gamma,k1;
print +s;
.end
#endif

	id gamma(m1?{9,12,13},?p1)=gamma(2,?p1);
	id gamma(2,p1?)=g_(2,p1);
	id gamma(2,mu1?)=g_(2,mu1);

***Second trace***
	tracen, 2;
endif;

#if 0
if (count(k1, 1) < 3);
  discard;
endif;
*id Pr(?p1) = 1;

id S=1;
id S0=1;

b gamma,k1;
print +s;
.end
#endif

id S=1;
id S0=1;
id T=1;
id T0=1;
id Pr(mu1?,mu2?,p1?,m?)=Pr(p1,m);

*#include kinematics5pt

id p5 = p1+p2-p3-p4;

id p1.p1 = mle^2;
id p2.p2 = mle^2;
id p3.p3 = mle^2;
id p4.p4 = mle^2;
*id p5.p5 = 0;
id e =1;

.sort

*mul replace_(s15,1,Mle,m);



*id Pr(?p1) = 1;
id d = 4 -2 *eps;
*id d = 4;

if (count(eps, 1) > 0);
  discard;
endif;

b Pr,k1	;

format mathematica;

print +s; 


.end
